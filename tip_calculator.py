"""Tip calculator, please run with base meal cost, tax % rate and tip % rate as arguements in that order"""
import sys
import pdb
#Tip Calculator thinkful.com exercise!
def rate_calc(base, percentage):
    if percentage >= 1:
        percentage = percentage/100
    return base*percentage

def meal_calc(b, t, ip):
    tax = rate_calc(b, t)
    b_w_tax = b + tax
    tip = rate_calc(b_w_tax, ip)
    total = b_w_tax + tip
    return {'tax':tax,'tip':tip, 'total':total}

def float_catch(n, var):
    while True:
        try:
            x = float(n)
            return x
        except ValueError:
            n = raw_input('Please enter a valid numeric value for the {}: '.format(var))

def main():
    try:
        b = float_catch(sys.argv[1], 'base cost')       
        t = float_catch(sys.argv[2], '% tax rate')
        ip = float_catch(sys.argv[3], '% tip rate')
    except IndexError:
        print 'ERROR: Please run with 3 arguments: meal cost, tax rate and tip rate.'
        exit(-1)
    meal = meal_calc(b, t, ip)
    print "Base meal cost: ${}".format(b)
    print "Tax: ${}".format(round(meal['tax'],2))
    print "Tip: ${}".format(round(meal['tip'],2))
    print "Total: ${}".format(round(meal['total'],2))

if __name__ == '__main__':
    main()
